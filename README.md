Open `populate_hashes.js`  
Change `serverSeed = 'DO NOT USE THIS SEED';` // see provably fair articles on how to create a seed

- Run below command to create hashes  

    `node populate_hashes.js`

Wait till it finish to 100% and stop after it print **Finished with serverseed: xxxxxxxxxx**

### Start locally

    npm start

    It will run game server on 127.0.0.1:3842


### on server

mkdir logs
forever start -o logs/out.log -e logs/err.log server.js
